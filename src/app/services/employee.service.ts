import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../shared/model/employee';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    private url = 'api/employees/';

    constructor(private http: HttpClient) {
    }

    getEmployees(): Observable<Employee[]> {
        return this.http.get<Employee[]>(this.url);
    }

    getEmployee(id: number): Observable<Employee> {
        return this.http.get<Employee>(this.url + id);
    }
}
