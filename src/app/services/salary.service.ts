import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Salary } from "../shared/model/salary";

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  private url = 'api/salaries/';

  constructor(private http: HttpClient) {
  }

  getSalaries(employeeNumber: number): Observable<Salary[]> {
    return this.http.get<Salary[]>(this.url + employeeNumber);
  }
}
