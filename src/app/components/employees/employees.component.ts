import { Component, OnInit } from '@angular/core';
import {Employee} from "../../shared/model/employee";
import {EmployeeService} from "../../services/employee.service";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[];
  displayedColumns: string[] = ['number', 'birthDate', 'firstName', 'lastName', 'gender', 'hireDate'];
  dataSource = new MatTableDataSource(this.employees);

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
        .subscribe(employees => {
          this.employees = employees;
          this.dataSource.data = this.employees;
        })
  }

}
