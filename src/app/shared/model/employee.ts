export class Employee {
    employeeNumber: number;
    birthDate: Date;
    firstName: string;
    lastName: string;
    gender: string;
    hireDate: Date;
}