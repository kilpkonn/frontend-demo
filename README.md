# Frontend demo
_by Tavo Annus_

**Backend can be found in separate [repo](https://gitlab.com/kilpkonn/backend-demo)**

## How to run locally
**Make sure you have:**
- Node 10 or newer

Clone repository
```bash
git clone https://gitlab.com/kilpkonn/frontend-demo.git && cd frontend-demo
```

Install node modules
```bash
npm install
```

Run frontend _(You need to have backend running for api!)_
```bash
ng serve
```
Frontend can be found at `http://localhost:4200/`