import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from "./components/employees/employees.component";
import { DetailComponent } from "./components/detail/detail.component";


const routes: Routes = [
    {
        path: ':id', component: DetailComponent
    },
    {
        path: '', component: EmployeesComponent
    }];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
