import { Component, OnInit } from '@angular/core';
import { Employee } from "../../shared/model/employee";
import { Salary } from "../../shared/model/salary";
import { EmployeeService } from "../../services/employee.service";
import { SalaryService } from "../../services/salary.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  employee: Employee;
  salaries: Salary[];
  displayedColumns: string[] = ['number', 'salary', 'fromDate', 'toDate'];
  dataSource = new MatTableDataSource(this.salaries);

  constructor(private route: ActivatedRoute,
              private router: Router,
              private employeeService: EmployeeService,
              private  salaryService: SalaryService) { }

  ngOnInit(): void {
    this.getEmployee()
  }

  getEmployee() {
    this.route.params.subscribe(params => {
      this.employeeService.getEmployee(params.id)
          .subscribe(employee => this.employee = employee);
      this.salaryService.getSalaries(params.id)
          .subscribe(salaries => {
            this.salaries = salaries;
            this.dataSource.data = this.salaries;
          })
    });
  }

}
