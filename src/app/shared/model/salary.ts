export class Salary {
    employeeNumber: number;
    salary: number;
    fromDate: Date;
    toDate: Date;
}